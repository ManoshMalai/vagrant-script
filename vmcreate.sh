#!/bin/bash
#
# vmhandle - This Tool help to setup Lab Environment very instantaneously. We can spin
#			 N no machine according to our requirment and the base machine hardware
#			 configuration's. we can also list and delete machine using this tool.
#           Written using Shell Script.

# USAGE: ./vmhanle
#
# REQUIREMENTS: Vagrant and Virtual Box
#
# OVERHEAD: Only when this tool is deployed for the first time it will download
#           a centos7 800MB Size image from Vagrant Cloud, thereafter for every new spin
#			the image already downloaded and available in the cache will be deployed.
#
#
# COPYRIGHT: Copyright (c) 2017 Manosh Malai.
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
#  (http://www.gnu.org/copyleft/gpl.html)
#
# 31-Aug-2017	Manosh Malai	Created this.

#Global Variable
Box_Name="geerlingguy/centos7"
#Box_Name="hashicorp/precise64"

[ -d /tmp/$USER ] || mkdir /tmp/$USER
export VAGRANT_HOME=/tmp/$USER
export VAGRANT_CWD=${HOME}

function Warn()
{
	if ! eval "$@"; then
		echo  >&2 "WARNING: Command Failed \"$@\""
	fi
}


#This Function use to spin new vms according to the user input.

function Creating_NewMachine()
{

	[ -f $PWD/Vagrantfile ] && Update_Existing_Vagrant_Env || Create_New_Vagrant_Env
}

function Chk_And_Add_VBox_To_LCache()
{
	#checking local vagrant cache
	if vagrant box list | grep -i ${Box_Name}  > /dev/null
	then
		echo " " > /dev/null
	else
		echo "Requested ${Box_Name} not existed on your local vagrant cache, so we are downloading for you. Please hold with us."
		Warn "vagrant box add ${Box_Name}" > /dev/null

		#After adding the box to our local cache checing its poperly added and printing the status to the user
		if vagrant box list | grep -i ${Box_Name}  > /dev/null
		then
			echo "${Box_Name} successfully added to you local cache"
		else
			echo "There's some issue in adding ${Box_Name} vagrant box to your local cache"
			exit 1
		fi
	fi
}

function Create_New_Vagrant_Env()
{
	starting_ip_oct=10
	read -p "Enter no of machine to spin: " no_machine
	
	#Calling Chk_And_Add_VBox_to_LCache function to check vagrant box exists on local cache. If not this function will try to add.
	Chk_And_Add_VBox_To_LCache


	cat  >> Vagrantfile <<EOL
	Vagrant.configure("2") do |config|
EOL
	for (( i=1; i <= $no_machine; i++))
	do
	ip=`expr $i + $starting_ip_oct`
	hostname="mydbopslabs$ip"
	cat  >> Vagrantfile <<EOL
		config.vm.define "$hostname" do |$hostname|
			${hostname}.vm.synced_folder "${HOME}", "/vagrant", type: "virtualbox"
			${hostname}.vm.box = "${Box_Name}"
			${hostname}.vm.network "private_network", ip: "192.168.33.${ip}"
			${hostname}.vm.hostname = "$hostname"
		end
EOL
	done
	cat >> Vagrantfile <<EOL
	end
EOL
	#Calling Power on machine function, to start the machine
	PowerOn_Machines
	#Installing vagrant-vbguest plugin, It will check and update the VirtualBox guest additions every time you run a vagrant up command
	Warn "vagrant plugin install vagrant-vbguest"
	List_UPVMS
	echo "You can login to the Vagrant Box using command \"vagrant ssh [name|id]\""
}

function Update_Existing_Vagrant_Env()
{
	starting_ip_oct=$(grep -io mydbopslabs[0-9][0-9] $PWD/Vagrantfile | sort -u | tr -dc '0-9' | tail -c 2)

    read -p "Enter no of machine to spin: " no_machine

	#MAC OSX "This sed comman use to delete last on the below specified file $ represent lastline d represent delete command"
	if [ $(uname) != "Linux" ]
	then
		sed -i '' '$d' Vagrantfile
	else
	#Linux "This sed comman use to delete last on the below specified file $ represent lastline d represent delete command"
		sed -i '$d' Vagrantfile
	fi

    for (( i=1; i <= $no_machine; i++))
    do
    ip=`expr $i + $starting_ip_oct`
    hostname="mydbopslabs$ip"
    cat  >> Vagrantfile <<EOL
        config.vm.define "$hostname" do |$hostname|
            ${hostname}.vm.synced_folder "${HOME}", "/vagrant", type: "virtualbox"
            ${hostname}.vm.box = "${Box_Name}"
            ${hostname}.vm.network "private_network", ip: "192.168.33.${ip}"
            ${hostname}.vm.hostname = "$hostname"
        end
EOL
    done
    cat >> Vagrantfile <<EOL
    end
EOL

	#Calling Chk_And_Add_VBox_to_LCache function to check vagrant box exists on local cache. If not this function will try to add.
	Chk_And_Add_VBox_To_LCache
	#Calling Power on machine function, to start the machine
	PowerOn_Machines
	#Installing vagrant-vbguest plugin, It will check and update the VirtualBox guest additions every time you run a vagrant up command
	Warn "vagrant plugin install vagrant-vbguest"
	List_UPVMS
	echo "You can login to the Vagrant Box using command \"vagrant ssh [name|id]\""
}

function Deleting_VMS()
{
	List_UPVMS
	listvmreturn=$?
	if [ $listvmreturn == 0 ]
	then
		read -p "Please select y(Yes) to Delete the Particular VMS or N(No) to delete all VMS:" ans

		case ${ans} in
			"y" | "yes" | "Y" | "YES" ) Deleting_Individual_VMS
			;;
			"n" | "no" | "N" | "NO" | "No" |"nO" ) Delete_All_Machine
			;;
			*) echo "Please enter correct option"
		esac
	fi
}

function Deleting_Individual_VMS()
{
	read -p "Enter the machine name to delete(you can specify mutiple machine by using space):" -a mname

	for i in ${mname[@]}
	do
		Warn "vagrant destroy -f ${i}"
	done

	if [ -f ${PWD}/Vagrantfile ]
	then
		echo "Machine deletion done. we are updating Vagrantfile, it will take very few min according to the config size"
		for i in ${mname[@]}
		do
			startline="config.vm.define \"$i\" do |${i}|"
			sed -i -e "/${startline}/,+5d" "${PWD}/Vagrantfile"
		done
	else
		echo" we can't find any Vagrantfile under Present Working Directory $PWD"
	fi
}

function Delete_All_Machine()
{
	Warn "vagrant destroy -f"
	Warn "rm -rf $PWD/Vagrantfile"
	echo "All Vagrant machines are deleted and Vagrantfile was removed"
}

function List_UPVMS()
{
	lsvmout=$(Warn "vagrant status" | grep -B10 'This\|The' | grep -v 'This\|The'| tee /dev/tty)
	grep -q "running" <<< $lsvmout
	if [ $? == 0 ]
	then
		return 0
	else
		echo "Check your Vagrantfile for more details"
	fi
}

function Halt_Vms()
{
	List_UPVMS
	listvmreturn=$?
	if [ $listvmreturn == 0 ]
	then
		read -p "Are you Planning to Shutdown all running machine, Please Press Y/n: " uchoice
		case ${uchoice} in
		       "Y" | "Yes"| "yes" | "y") Warn "vagrant halt"
				;;
			   "N" | "No" | "no" | "n") read -p "Please enter the machine name to Shutdown: " mname ; Warn "vagrant halt ${mname}"
				;;
				*) echo "Please enter correct option!!!"
				;;
		esac
	fi
}

function PowerOn_Individual_Machines()
{
	read -p "Please enter the machine name to Power On (you can specify mutiple machine by using space):" -a mname
	for i in ${mname[@]}
	do
		Warn "vagrant up ${i}"
	done
}

function PowerOn_Machines()
{
	List_UPVMS
	listvmreturn=$?
	if [ $listvmreturn == 0 ]
	then
		read -p "Are you Planning to Power up all machine's, Please Press Y/n: " uchoice
		case ${uchoice} in
		       "Y" | "Yes"| "yes" | "y") vagrant up
				;;
			   "N" | "No" | "no" | "n") PowerOn_Individual_Machines
				;;
				*) echo "Please enter correct option!!!"
				;;
		esac
	fi

}

function Restart_Machines()
{
	List_UPVMS
	listvmreturn=$?
	if [ $listvmreturn == 0 ]
	then
		read -p "Are you Planning to Restart up all machines, Please Press Y/n: " uchoice
		case ${uchoice} in
		       "Y" | "Yes"| "yes" | "y") vagrant reload
				;;
			   "N" | "No" | "no" | "n") reload_Individual_Machines
				;;
				*) echo "Please enter correct option!!!"
				;;
		esac
	fi
}

function reload_Individual_Machines()
{
	read -p "Please enter the machine name to restart (you can specify mutiple machine by using space):" -a mname
	for i in ${mname[@]}
	do
		Warn "vagrant reload ${i}"
	done

}

function Exit_Script()
{
	exit 0
}


#Main Script Start Here

type -P vagrant &> /dev/null && echo "Found" >> /dev/null || if [ "$(id -u)" != "0" ]; then

 eval "$(echo "
#Vagrant Not installed on your machine, kindly follow the below step to install the same.

# Some notes from installing VirtualBox on CentOS 7.
# These exact steps haven't been tested, as I ran them in a different order when manually installing.

# Install dependencies
yum -y install gcc make patch  dkms qt libgomp
yum -y install kernel-headers kernel-devel fontforge binutils glibc-headers glibc-devel

# Install VirtualBox
cd /etc/yum.repos.d
wget http://download.virtualbox.org/virtualbox/rpm/rhel/virtualbox.repo
yum -y install VirtualBox-5.0

# Check that you have the kernel sources downloaded for your running kernel version (if they don't match you might need to yum update and reboot)
ls /usr/src/kernels/
uname -r

# Build the VirtualBox kernel module
export KERN_DIR=/usr/src/kernels/$(uname -r)
/sbin/rcvboxdrv setup

# Install Vagrant
yum -y install https://releases.hashicorp.com/vagrant/1.8.1/vagrant_1.8.1_x86_64.rpm
")"
else
	echo "Vagrant not installed on this machine, Installion only done by root user so please execute this script as root or kindly check with your System Team"
	exit 1
fi



echo "Welcome to managing portal for you Lab Environment"

echo "1. Creating VM"
echo "2. Delete VMS"
echo "3. List UP VMS"
echo "4. Shutdown VM"
echo "5. Power On Machines"
echo "6. Restart Machines"
echo "7. Exit"
read -p "select your options:" OPTIONS

case ${OPTIONS} in
	1) Creating_NewMachine ;;
	2) Deleting_VMS ;;
	3) List_UPVMS ;;
	4) Halt_Vms ;;
	5) PowerOn_Machines ;;
	6) Restart_Machines ;;
	7) Exit_Script ;;
    *) echo "Please select correct options" ;;
esac
